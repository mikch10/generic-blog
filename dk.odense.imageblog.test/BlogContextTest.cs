﻿using dk.odense.imageblog.model;
using dk.odense.imageblog.persistence;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dk.odense.imageblog.test
{
    [TestClass]
    public class BlogContextTest
    {
        public BlogContext ctx { get; set; }
        public BlogContextTest()
        {
            ctx = new BlogContext(new Microsoft.EntityFrameworkCore.DbContextOptions<BlogContext>());
        }
        
        [TestMethod]
        public void HasDbSetForComments()
        {
            var props = ctx.GetType().GetProperties();
            var shouldBeTrue = props.Any(p => p.PropertyType.IsGenericType && p.PropertyType.GetGenericArguments()[0] == new Comment().GetType());
            Assert.IsTrue(shouldBeTrue, "Must have a DbSet of Type Comment");
        }

        [TestMethod]
        public void HasDbSetForBlogs()
        {
            var props = ctx.GetType().GetProperties();
            var shouldBeTrue = props.Any(p => p.PropertyType.IsGenericType && p.PropertyType.GetGenericArguments()[0] == new Blog().GetType());
            Assert.IsTrue(shouldBeTrue, "Must have a DbSet of Type Blog");
        }

        [TestMethod]
        public void HasDbSetForPosts()
        {
            var props = ctx.GetType().GetProperties();
            var shouldBeTrue = props.Any(p => p.PropertyType.IsGenericType && p.PropertyType.GetGenericArguments()[0] == new Post().GetType());
            Assert.IsTrue(shouldBeTrue, "Must have a DbSet of Type Post");
        }

    }
}
