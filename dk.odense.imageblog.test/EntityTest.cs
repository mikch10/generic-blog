using dk.odense.imageblog.model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Reflection;

namespace dk.odense.imageblog.test
{
    [TestClass]
    public class EntityTest
    {
        Entity Entity { get; set; }
        DateTime Date { get; set; }
        public EntityTest()
        {
            Entity = new Entity();
            Date = new DateTime();
        }

        [TestMethod]
        public void EntityHasPostDate()
        {
            Assert.IsTrue(Entity.GetType().GetProperties().Any(e => e.Name == "PostDate"), "Entity must have a public property PostDate.");
        }

        [TestMethod]
        public void PostDateIsTypeDate()
        {
            Assert.IsTrue(Entity.GetType().GetProperty("PostDate").PropertyType == DateTime.Now.GetType(), "PostDate must be of type DateTime");
        }

        [TestMethod]
        public void EntityHasAuthorField()
        {
            Assert.IsTrue(Entity.GetType().GetProperties().Any(e => e.Name == "Author"), "Entity must have a public property Author.");
        }

        [TestMethod]
        public void AuthorFieldIsString()
        {
            Assert.IsTrue(Entity.GetType().GetProperty("Author").PropertyType == "".GetType(), "Author property must be of type string.");
        }

        [TestMethod]
        public void HasNoPropertyNamedMessage()
        {
            Assert.IsFalse(Entity.GetType().GetProperties().Any(p => p.Name == "Message"));
        }

        [TestMethod]
        public void HasNoPropertyNamedContent()
        {
            Assert.IsFalse(Entity.GetType().GetProperties().Any(p => p.Name == "Content"));
        }

        [TestMethod]
        public void HasDefinedIdOnSelf()
        {
            Type t = Entity.GetType();
            var isTrue = t.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance).Any(p => p.PropertyType == typeof(Guid) && p.Name == "Id");
            Assert.IsTrue(isTrue, "Id property must be defined on this class.");
        }

    }
}
