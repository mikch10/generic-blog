﻿using dk.odense.imageblog.model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace dk.odense.imageblog.test
{
    [TestClass]
    public class BlogTest
    {
        Blog Blog { get; set; }
        public BlogTest()
        {
            Blog = new Blog();
        }

        [TestMethod]
        public void InheritsEntity()
        {
            Assert.IsInstanceOfType(Blog, new Entity().GetType(), "Blog must inherit from Entity");
        }

        [TestMethod]
        public void HasPropertyNamedContent()
        {
            Assert.IsTrue(Blog.GetType().GetProperties().Any(p => p.Name == "Content"));
        }

        [TestMethod]
        public void HasNotDefinedIdOnSelf()
        {
            Type t = Blog.GetType();
            var isFalse = t.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance).Any(p => p.PropertyType == typeof(Guid) && p.Name == "Id");
            Assert.IsFalse(isFalse, "Id property must be inherited.");
        }

        [TestMethod]
        public void HasPropertyId()
        {
            Type t = Blog.GetType();
            var isTrue = t.GetProperties().Any(p => p.PropertyType == typeof(Guid) && p.Name == "Id");
            Assert.IsTrue(isTrue, "Id property must exist.");
        }

    }
}
