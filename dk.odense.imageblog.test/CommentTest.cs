﻿using dk.odense.imageblog.model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace dk.odense.imageblog.test
{
    [TestClass]
    public class CommentTest
    {
        Comment Comment { get; set; }
        public CommentTest()
        {
            Comment = new Comment();
        }

        [TestMethod]
        public void HasMessageField()
        {
            Assert.IsTrue(Comment.GetType().GetProperties().Any(e => e.Name == "Message"), "Comment must have a public property Message.");
        }

        [TestMethod]
        public void MessageIsString()
        {
            Assert.IsTrue(Comment.GetType().GetProperty("Message").PropertyType == "".GetType(), "Message property must be of type string.");
        }

        [TestMethod]
        public void InheritsEntity()
        {
            Assert.IsInstanceOfType(Comment, new Entity().GetType(), "Comment must inherit from Entity");
        }

        [TestMethod]
        public void HasNotDefinedIdOnSelf()
        {
            Type t = Comment.GetType();
            var isFalse = t.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance).Any(p => p.PropertyType == typeof(Guid) && p.Name == "Id");
            Assert.IsFalse(isFalse, "Id property must be inherited.");
        }

        [TestMethod]
        public void HasPropertyId()
        {
            Type t = Comment.GetType();
            var isTrue = t.GetProperties().Any(p => p.PropertyType == typeof(Guid) && p.Name == "Id");
            Assert.IsTrue(isTrue, "Id property must exist and must be of type Guid");
        }

    }
}
