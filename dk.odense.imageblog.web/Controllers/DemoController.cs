﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace dk.odense.imageblog.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DemoController : ControllerBase
    {
        List<string> SentenceList { get; set; }
        public DemoController()
        {
            SentenceList = new List<string>();
            AddSentences();
        }

        private void AddSentences()
        {
            SentenceList.Add(@"I'd love to see a video of how it works.");
            SentenceList.Add(@"Neat. I want to make love to the use of style and background image!");
            SentenceList.Add(@"My 60 year old dad rates this shapes very neat.");
            SentenceList.Add(@"This shot has navigated right into my heart.");
            SentenceList.Add(@"I want to learn this kind of work! Teach me.");
            SentenceList.Add(@"Good. So magical.");
            SentenceList.Add(@"Tremendously neat animation dude");
            SentenceList.Add(@"Let me take a nap... great colour palette, anyway.");
            SentenceList.Add(@"Delightful animation :-)");
            SentenceList.Add(@"This atmosphere blew my mind.");
            SentenceList.Add(@"Found myself staring at it for minutes.");
            SentenceList.Add(@"Outstandingly thought out! You just won the internet!");
            SentenceList.Add(@"My 74 year old grandson rates this icons very slick :)");
            SentenceList.Add(@"My 51 year old son rates this concept very bold :)");
            SentenceList.Add(@"Whoa.");
            SentenceList.Add(@"Such colour palette, many blur, so amazing");
            SentenceList.Add(@"I want to learn this kind of layers! Teach me.");
            SentenceList.Add(@"Such fab.");
            SentenceList.Add(@"Mission accomplished. It's appealing.");
            SentenceList.Add(@"Grey. How do you make this? Photoshop?");
            SentenceList.Add(@"Tremendously thought out! I wonder what would have happened if I made this");
            SentenceList.Add(@"This shot has navigated right into my heart.");
        }

        [HttpGet("random")]
        public string GetRandom()
        {
            Random r = new Random();
            return SentenceList[r.Next(0, SentenceList.Count)];
        }

    }
}