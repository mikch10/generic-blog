﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dk.odense.imageblog.model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace dk.odense.imageblog.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {

        public CommentController()
        {
            // add services.
        }


        [HttpGet("{id}")]
        public Comment GetComment(Guid id)
        {
            /**
            * Implement the code which retrieves a comment by its ID
            */
            return null;
        }

        [HttpGet]
        public IEnumerable<Comment> GetComments()
        {
            /*
             * Implement the code which retrieves a list of all comments.
             */
            return null;
        }

        [HttpPut("{id}")]
        public bool UpdateComment(Guid id, [FromBody] Comment comment)
        {
            /*
             * Implement the code which updates an existing comment in the database.
             */
            return false;
        }

        [HttpPost]
        public bool PostComment(Comment comment)
        {
            /*
             * Implement the code which posts a new comment and saves it in the database.
             */
            return false;
        }

        [HttpDelete("{id}")]
        public bool DeleteComment(Guid id)
        {
            /*
             * Implement the code which deletes a comment by id.
             */
            return false;
        }

    }
}