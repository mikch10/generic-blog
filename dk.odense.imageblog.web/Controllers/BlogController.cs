﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dk.odense.imageblog.model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace dk.odense.imageblog.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlogController : ControllerBase
    {

        public BlogController()
        {
            // add services.
        }

        [HttpGet("{id}")]
        public Blog GetBlog(Guid id)
        {
            /**
            * Implement the code which retrieves a blog by its ID
            */
            return null;
        }

        [HttpGet]
        public IEnumerable<Blog> GetBlogs()
        {
            /*
             * Implement the code which retrieves a list of all blogs.
             */
            return null;
        }

        [HttpPut("{id}")]
        public bool UpdateBlog(Guid id, [FromBody] Blog blog)
        {
            /*
             * Implement the code which updates an existing blog in the database.
             */
            return false;
        }

        [HttpPost]
        public bool PostBlog(Blog blog)
        {
            /*
             * Implement the code which posts a new blog and saves it in the database.
             */
            return false;
        }

        [HttpDelete("{id}")]
        public bool DeleteBlog(Guid id)
        {
            /*
             * Implement the code which deletes a blog and all of its associated comments.
             */
            return false;
        }
    }
}