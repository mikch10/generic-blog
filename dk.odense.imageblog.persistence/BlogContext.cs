﻿using dk.odense.imageblog.model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace dk.odense.imageblog.persistence
{
    public class BlogContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        //Add the missing DbSets here

        //End of edits.
        public BlogContext(DbContextOptions<BlogContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        } 

    }
}
