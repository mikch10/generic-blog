﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace dk.odense.imageblog.model
{
    public class Comment
    {
        public Guid PostId { get; set; }
        [ForeignKey("PostId")]
        public Post Post { get; set; }
    }
}
