﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dk.odense.imageblog.model
{
    public class Blog
    {
        public Guid Id { get; set; }
        public ICollection<Post> Posts { get; set; }
    }
}
